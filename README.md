# grpc-example

This is a simple implementation of a gRPC api that uses a golang server and
a C# client. Great for interfacing with legacy applications...

### Protofiles
The application has needs proto files for both golang and C#.  Information on
downloading and using the protobuf compiler can be found [here](https://developers.google.com/protocol-buffers/docs/tutorials).

Once protoc and the golang protoc extension is installed, the following commands
can be used to generate new proto files:

```
cd grpc-example/
protoc proto/src/grpcSkeleton/grpcSkeleton.proto --charp_out="client/"
protoc --go_out=plugins=grpc:. proto/src/grpcSkeleton/grpcSkeleton.proto
```

### Golang Server

To build a the golang server use the following command:

```
cd grpc-example/server/
go run main.go
```

### Csharp Client
To build the C# client use the following commands:

```
cd grpc-example/client/proto
dotnet build
cd ../FakeServer
dotnet build
cd ../Client
dotnet build
```

To run the client, run the following command:
```
cd grpc-example/client/Client/
dotnet run
```

### Troubleshooting
If on a second start the Golang server does not start run the following command:
```
ps -af | grep go
```
Then run the following command on the ports that appear in the second column:
```
kill -9 <port>
```
You should now be able to start the golang server again.

package main

import (
  "context"
  "log"
  "net"

  "google.golang.org/grpc"
  pb "grpcSkeleton"
)

const (
  port = "0.0.0.0:50051"
)

type server struct {
  pb.UnimplementedStatusPingServer
}

func (s *server) PingServer(ctx context.Context, in *pb.Ping) (*pb.PingReply, error) {
  log.Printf("Received: %v", in.GetName())
  return &pb.PingReply{Message: "Hello: " + in.GetName()}, nil
}

func main() {
  lis, err := net.Listen("tcp", port)
  if err != nil {
    log.Fatalf("failed to listen: %v", err)
  }
  s := grpc.NewServer()
  pb.RegisterStatusPingServer(s, &server{})
  if err :=  s.Serve(lis); err != nil {
    log.Fatalf("failed to serve: %v", err)
  }
}

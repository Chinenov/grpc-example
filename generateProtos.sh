#!/bin/bash
protoc proto/src/grpcSkeleton/grpcSkeleton.proto --charp_out="client/"
protoc --go_out=plugins=grpc:proto/src/grpcSkeleton/ proto/src/grpcSkeleton/grpcSkeleton.proto

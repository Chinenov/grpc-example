using System;
using System.Threading.Tasks;
using Grpc.Core;
using GrpcSkeleton;

namespace FakeServer
{
  class StatusPingImpl: StatusPing.StatusPingBase
  {
    public override Task<PingReply> PingServer(Ping request, ServerCallContext context)
    {
      return Task.FromResult(new PingReply { Message = "Hello " + request.Name});
    }
  }

  class Program
  {
    const int Port = 50051;
    public static void Main(string[] args)
    {
      Server server = new Server
      {
        Services = { StatusPing.BindService(new StatusPingImpl())},
        Ports = { new ServerPort("0.0.0.0", Port, ServerCredentials.Insecure) }
      };
      server.Start();

      Console.WriteLine("Greeter server listening on port " +Port);
      Console.WriteLine("Press any key to stop the server...");
      Console.ReadKey();

      server.ShutdownAsync().Wait();
    }
  }
}

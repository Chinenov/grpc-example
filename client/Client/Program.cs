using System;
using Grpc.Core;
using GrpcSkeleton;

namespace GrpcSkeletonClient
{
  class Program
  {
    public static void Main(string[] args)
    {
      Channel channel = new Channel("0.0.0.0:50051", ChannelCredentials.Insecure);

      var client = new StatusPing.StatusPingClient(channel);
      String user = "you";

      var reply = client.PingServer(new Ping { Name = user });
      Console.WriteLine("Greeting: " + reply.Message);

      channel.ShutdownAsync().Wait();
      Console.WriteLine("Press any key to exit...");
      Console.ReadKey();
    }
  }
}
